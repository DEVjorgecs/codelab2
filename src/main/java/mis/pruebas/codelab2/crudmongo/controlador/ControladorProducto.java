package mis.pruebas.codelab2.crudmongo.controlador;



import mis.pruebas.codelab2.crudmongo.modelo.Producto;
import mis.pruebas.codelab2.crudmongo.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    // Obtiene todos los productos
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        System.out.println("Busca productos en mongo");
        return servicioProducto.findAll();
    }

    // Obtiene productos por Id
    @GetMapping("/productos/{id}" )
    public Optional<Producto> getProductoId(@PathVariable String id){
        System.out.println("Busca producto en mongo por ID");
        return servicioProducto.findById(id);
    }

    // Agrega nuevos productos
    @PostMapping("/productos")
    public Producto postProductos(@RequestBody Producto newProducto){
        System.out.println("agrego producto en mongo");
        servicioProducto.save(newProducto);
        return newProducto;
    }

    // Actualiza nuevos productos
    @PutMapping("/productos")
    public void putProductos(@RequestBody Producto productoToUpdate){
        System.out.println("actualizo producto en mongo");
        servicioProducto.save(productoToUpdate);
    }

    // Elimina productos
    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody Producto productoToDelete){
        System.out.println("se bora el producto en mongo");
        return servicioProducto.delete(productoToDelete);
    }

}

