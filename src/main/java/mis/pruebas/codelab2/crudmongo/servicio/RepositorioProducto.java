package mis.pruebas.codelab2.crudmongo.servicio;

import mis.pruebas.codelab2.crudmongo.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String> {
}
